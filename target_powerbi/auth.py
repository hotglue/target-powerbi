import json
import requests
from datetime import datetime, timedelta, timezone


class MicrosoftAuth(requests.auth.AuthBase):
    def __init__(self, config):
        self.__config = config
        # self.__config_path = parsed_args.config_path
        self.__resource = "https://api.powerbi.com/v1.0/myorg/"
        self.__client_id = config["client_id"]
        self.__client_secret = config["client_secret"]
        self.__redirect_uri = config["redirect_uri"]
        self.__refresh_token = config["refresh_token"]

        self.__session = requests.Session()
        self.__access_token = None
        self.__expires_at = None

    def update_access_token(self):
        if self.__access_token is None or self.__expires_at <= datetime.utcnow():
            response = self.__session.post(
                "https://login.microsoftonline.com/common/oauth2/token",
                data={
                    "client_id": self.__client_id,
                    "client_secret": self.__client_secret,
                    "redirect_uri": self.__redirect_uri,
                    "refresh_token": self.__refresh_token,
                    "grant_type": "refresh_token",
                },
            )

            if response.status_code != 200:
                raise Exception(response.text)

            data = response.json()

            self.__access_token = data["access_token"]
            self.__config["refresh_token"] = data["refresh_token"]
            self.__config["expires_in"] = data["expires_in"]
            self.__config["access_token"] = data["access_token"]

            with open("config.json", "w") as outfile:
                json.dump(self.__config, outfile, indent=4)

            self.__expires_at = datetime.utcnow() + timedelta(
                seconds=int(data["expires_in"]) - 10
            )  # pad by 10 seconds for clock drift

    def is_token_valid(self) -> bool:
        access_token = self.__config.get("access_token")
        now = round(datetime.now(timezone.utc).timestamp())
        expires_in = self.__config.get("expires_in")
        if  expires_in is not None:
            expires_in = int(expires_in)
        if not access_token:
            return False

        if not expires_in:
            return False

        return not ((expires_in - now) < 120)
    
    def get_access_token(self) -> dict:
        if not self.is_token_valid():
            self.update_access_token()
        return self.__access_token
    
    def __call__(self, r):
        r.headers["Authorization"] = f"Bearer {self.get_access_token()}"
        return r