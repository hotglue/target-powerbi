"""PowerBI target class."""

from singer_sdk import typing as th
from singer_sdk.target_base import Target

from target_powerbi.sinks import PowerBISink


class TargetPowerBI(Target):
    """Sample target for PowerBI."""

    name = "target-powerbi"
    config_jsonschema = th.PropertiesList(
        th.Property("client_id", th.StringType, required=True),
        th.Property("client_secret", th.StringType, required=True),
        th.Property("redirect_uri", th.StringType, required=True),
        th.Property("refresh_token", th.StringType, required=True),
    ).to_dict()
    default_sink_class = PowerBISink


if __name__ == "__main__":
    TargetPowerBI.cli()
