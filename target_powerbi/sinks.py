import json
from datetime import datetime
from typing import Dict, List, Optional

import requests
from singer_sdk.plugin_base import PluginBase
from singer_sdk.sinks import RecordSink

from target_powerbi.auth import MicrosoftAuth


class PowerBISink(RecordSink):
    """PowerBI target sink class."""

    def __init__(
        self,
        target: PluginBase,
        stream_name: str,
        schema: Dict,
        key_properties: Optional[List[str]],
    ) -> None:
        super().__init__(target, stream_name, schema, key_properties)
        # Save config for refresh_token saving
        self.config_file = target.config
        self.target_name = "dynamics"
        self.url = "https://api.powerbi.com/v1.0/myorg/"
        self.datasets = self.get_datasets()

        dataset_ids = self.search_datasets()
        if self.config.get("dataset_id"):
            self.dataset_id = self.config.get("dataset_id")
        elif len(dataset_ids) > 0:
            self.dataset_id = dataset_ids[0]['id']
        else:
            columns = []
            self.build_table_schema(schema['properties'],columns)

            payload = {
                "name": self.stream_name,
                "defaultMode":"Push",
                "tables": [
                    {
                        'name': self.stream_name,
                        'columns': columns,
                    }
                ]

            }
            response = self.push_dataset(payload)

            if response.status_code not in [200, 201]:
                raise Exception(response.text)

            self.dataset_id = response.json()['id']

    def get_auth(self):
        auth = MicrosoftAuth(dict(self.config))
        r = requests.Session()
        auth = auth(r)
        return auth

    def push_dataset_rows(self, record,stream_name):
        table_name = self.config.get("table_name",stream_name)
        auth = self.get_auth()
        url = self.url
        if self.config.get('workspace_id'):
            url = f"{url}groups/{self.config.get('workspace_id')}/datasets/{self.dataset_id}/tables/{table_name}/rows"
        else:    
            url = f"{url}datasets/{self.dataset_id}/tables/{table_name}/rows"
        payload = {'rows':record}
        res = auth.post(url, json=payload)
        if res.status_code == 404 and table_name in res.text and "not found" in res.text:
            #add table
            # API does not support patching / adding table to existing dataset
            # tables_payload = {"tables":
            #     [
            #         {
            #             'name': table_name,
            #             'columns': []
            #         }
            #     ]
            # }
            # self.push_dataset(tables_payload, self.dataset_id)
            self.logger.warn(f"Table {table_name} not found in the dataset {self.dataset_id}, skipping...")
        return res

    def push_dataset(self, record,dataset_id=None):
        auth = self.get_auth()
        url = self.url
        #Push dataset to a workplace
        if self.config.get("workspace_id"):
            url = f"{url}groups/{self.config.get('workspace_id')}/datasets" 
        else:       
            url = f"{url}datasets"
        if not dataset_id:    
            res = auth.post(url, json=record)
        elif dataset_id:
            url = f"{url}/{dataset_id}"
            res = auth.patch(url, json=record)    
        return res

    def build_table_schema(self,schema,columns):
        for item in schema.keys():
            type = schema[item]['type']
            format = schema[item].get("format")
            column_value = {}
            column_value['name'] = item
            if "number" in type:
                column_value['dataType'] = 'Decimal'
            elif "integer" in type:
                column_value['dataType'] = 'Int64'
            elif format == "date-time":
                column_value['dataType'] = "DateTime"
            elif "string" in type:
                column_value['dataType'] = "string"
            elif "bool" in type or "boolean" in type:
                column_value['dataType'] = "bool"
            elif "object" in type or "array" in type:
                column_value['dataType'] = "string"
            else:
                column_value['dataType'] = "string"


            columns.append(column_value)

    def get_datasets(self):
        try:
            auth = self.get_auth()
            url = self.url
            if self.config.get('workspace_id'):
                url = f"{url}groups/{self.config.get('workspace_id')}/datasets"
            else:
                url = f"https://api.powerbi.com/v1.0/myorg/datasets"
            res = auth.get(url)
            return res.json()['value']
        except Exception as exc:
            raise Exception(f"Failing during get datasets. Status code: {res.status_code}. " + \
                              f"Workspace ID {self.config.get('workspace_id')}" + \
                              f". Error: {str(exc)}. Response: {res.text}") from exc


    def search_datasets(self):
        return list(filter(lambda x: x['name'] == self.stream_name,self.datasets))


    def process_record(self, record: dict, context: dict) -> None:
        for k,v in record.items():
            if isinstance(v,datetime):
                v = v.isoformat()
                record[k] = v
            if type(v) == list or type(v) == dict:
                record[k] = json.dumps(v)

        resp = self.push_dataset_rows([record],self.stream_name)


